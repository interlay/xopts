Welcome to XOpts' documentation!
================================


.. toctree::
   :maxdepth: 2
   :caption: Introduction

   intro/at-a-glance
   intro/protocols
   intro/architecture
   intro/user-guide

.. toctree::
   :maxdepth: 2
   :caption: Specification

   spec/optionpool
   spec/optionpair
   spec/sell-side-option
   spec/buy-side-option
   spec/bitcoin-tx
   spec/btc-relay

.. toctree::
   :maxdepth: 2
   :caption: Integration

   integration/uniswap

.. toctree::
   :maxdepth: 2
   :caption: All the rest

   other/license
   other/interlay
