User Guide
==========

We imagine most interactions with XOpts will happen through either its :ref:`website` or the TypeScript library :ref:`xopts-js`.
If you are interested to integrate directly with our smart contracts, you can check out the full specification in this documentation.
Otherwise, you can find the user guides for the website and TS library below.

.. _website:

Website
-------

XOpts can be used through the [official website](https://xopts.io/). A detailed [user guide is available there](https://xopts.io/help).

.. _xopts-js:

xopts-js
--------

Work-in-progress.
