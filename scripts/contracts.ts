import { ethers } from "@nomiclabs/buidler";
import { Signer, Wallet, Contract } from "ethers";
import { legos } from "@studydefi/money-legos";
import { CollateralFactory } from "../typechain/CollateralFactory";
import { OptionPoolFactory } from "../typechain/OptionPoolFactory";
import { contracts } from "../contracts";
import { MockRelayFactory } from "../typechain/MockRelayFactory";
import { MockTxValidatorFactory } from "../typechain/MockTxValidatorFactory";
import { TxValidatorFactory } from "../typechain/TxValidatorFactory";
import { ERC137ResolverFactory } from "../typechain/ERC137ResolverFactory";
import { ERC137RegistryFactory } from "../typechain/ERC137RegistryFactory";
import { ERC20SellableFactory } from "../typechain/ERC20SellableFactory";
import { ERC20BuyableFactory } from "../typechain/ERC20BuyableFactory";
import IUniswapV2Factory from '@uniswap/v2-core/build/IUniswapV2Factory.json'
import IUniswapV2ERC20 from '@uniswap/v2-core/build/IUniswapV2ERC20.json'
import IUniswapV2Pair from '@uniswap/v2-core/build/IUniswapV2Pair.json'
import IUniswapV2Router01 from '@uniswap/v2-periphery/build/IUniswapV2Router01.json'
import { BigNumber } from "ethers/utils";
import { IERC20 } from "../typechain/IERC20";
import { IERC20Buyable } from "../typechain/IERC20Buyable";
import { Token, Pair, TokenAmount } from "@uniswap/sdk";

let overrides = {
  gasPrice: ethers.utils.parseUnits('20.0', 'gwei'),
}

interface Callable {
	address: string;
}

interface Attachable<C> {
	attach(addr: string): C;
}

export function call<A extends Callable, B extends Attachable<A>>(contract: A, factory: new (from: Signer) => B, signer: Signer): A {
	let _factory = new factory(signer);
	return _factory.attach(contract.address);
}

export async function mintDai(collateral: Contract, userAddress: string, collateralAmount: string) {
    let signer = ethers.provider.getSigner(contracts.ropsten.dai_account);
    let fromDaiAccount = collateral.connect(signer);

    await fromDaiAccount.transfer(userAddress, collateralAmount);
}

export function attachSellableOption(signer: Signer, address: string) {
	return new ERC20SellableFactory(signer).attach(address);
}

export function attachBuyableOption(signer: Signer, address: string) {
	return new ERC20BuyableFactory(signer).attach(address);
}

// convert a BTC amount to satoshis
export function btcToSatoshi(amount: number) {
    return amount * 100_000_000;
}

// convert a BTC amount to satoshis
export function mbtcToSatoshi(amount: number) {
    return amount * 100_000;
}

// convert satoshis to mBTC
export function satoshiToMbtc(amount: number) {
    return Math.round(amount / 100_000);
}

// convert dai to weiDai
export function daiToWeiDai(amount: number) {
    return ethers.utils.parseEther(amount.toString());
}

// convert dai to weiDai
export function mdaiToWeiDai(amount: number) {
    let dai = amount / 1000;
    return daiToWeiDai(dai);
}

// convert weiDai to mDai
export function weiDaiToMdai(amount: string) {
    return ethers.utils.formatUnits(amount, 15);
}

// calculate the premium in dai for 1 BTC
export function premiumInDaiForOneBTC(amount: number) {
    let weiDai = daiToWeiDai(amount);
    return weiDai.div(btcToSatoshi(1));
}

// calculate the premium in dai for 1 BTC
export function strikePriceInDaiForOneBTC(amount: number) {
    let weiDai = daiToWeiDai(amount);
    return weiDai.div(btcToSatoshi(1));
}

// use Dai addresses
export async function Collateral() {
    // if we use the ganache forking option, use the collateral address on Ropsten
	const dai = contracts.ropsten.dai;
    const collateral = await ethers.getContractAt(legos.erc20.abi, dai);
	console.log("Collateral (Dai)", dai);
	return collateral;
}

// Uniswap factory
export async function createUniswapPair(signer: Signer, tokenA: string, tokenB: string) {
    const abi = IUniswapV2Factory.abi;
    const factory = await ethers.getContractAt(abi, "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f", signer);
    await factory.createPair(tokenA, tokenB);
}

// Uniswap router
export async function addLiquidity(
    signer: Signer,
    tokenA: IERC20,
    tokenB: IERC20Buyable,
    amountADesired: BigNumber,
    amountBDesired: BigNumber,
    amountAMin: number,
    amountBMin: number,
    account: string,
    deadline: number,
    pairAddress: string,
) {
    // const abi = IUniswapV2Router01.abi;
    // const router = await ethers.getContractAt(abi, "0xf164fC0Ec4E93095b804a4795bBe1e041497b92a", signer);
    // await router.addLiquidity(
    //     tokenA.address,
    //     tokenB.address,
    //     amountADesired,
    //     amountBDesired,
    //     amountAMin,
    //     amountBMin,
    //     account,
    //     deadline
    // );

    await tokenA.transfer(pairAddress, amountADesired);
    await tokenB.transfer(pairAddress, amountBDesired);
    const abi = IUniswapV2Pair.abi;
    const exchange = await ethers.getContractAt(abi, pairAddress, signer);
    await exchange.mint(account);
}

export async function fetchData(
    signer: Signer,
    tokenA: Token,
    tokenB: Token,
    pairAddress: string
) {
    const pair = await ethers.getContractAt(IUniswapV2Pair.abi, pairAddress, signer);
    const [reserves0, reserves1] = await pair.getReserves();
    const balances = tokenA.sortsBefore(tokenB) ? [reserves0, reserves1] : [reserves1, reserves0]
    return new Pair(new TokenAmount(tokenA, balances[0]), new TokenAmount(tokenB, balances[1]))
}

export async function MockCollateral(signer: Signer) {
	let factory = new CollateralFactory(signer);
	let contract = await factory.deploy(overrides);
	console.log("Collateral (ERC20):", contract.address);
	return contract.deployed();
}

export async function MockRelay(signer: Signer) {
    let factory = new MockRelayFactory(signer);
	let contract = await factory.deploy();
	console.log("MockRelay contract:", contract.address);
	return contract.deployed();
}

export async function MockTxValidator(signer: Signer) {
    let factory = new MockTxValidatorFactory(signer);
	let contract = await factory.deploy();
	console.log("MockTxValidator contract:", contract.address);
	return contract.deployed();
}

export async function TxValidator(signer: Signer) {
    let factory = new TxValidatorFactory(signer);
	let contract = await factory.deploy(overrides);
	console.log("TxValidator contract:", contract.address);
	return contract.deployed();
}

export async function MockRegistryAndResolver(signer: Signer) {
    let resolverFactory = new ERC137ResolverFactory(signer);
    let resolver = await resolverFactory.deploy(await signer.getAddress());

    let registryFactory = new ERC137RegistryFactory(signer);
    let registry = await registryFactory.deploy();
    registry.setResolver(Buffer.alloc(32).fill(0), resolver.address);
	return registry.deployed();
}

export async function OptionPool(signer: Signer, collateral: string, relay: string, valid: string) {
  let factory = new OptionPoolFactory(signer);
	let contract = await factory.deploy(collateral, relay, valid, overrides);
	console.log("OptionPool contract:", contract.address);
	return contract.deployed();
}

export async function getBuyableAndSellable(sellableAddress: string, signer: Signer) {
    let sellableFactory = new ERC20SellableFactory(signer);
    let sellableContract = sellableFactory.attach(sellableAddress);
    let buyableAddress = await sellableContract.getBuyable();
    let buyableFactory = new ERC20BuyableFactory(signer);
    let buyableContract = buyableFactory.attach(buyableAddress);
    return {sellableContract, buyableContract};
}